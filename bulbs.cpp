int Solution::bulbs(vector<int> &A) {
    int n_switches = 0;
    int curr_pos = 0;

    while (curr_pos < A.size()) {
        if (A[curr_pos] == (n_switches % 2)) {
            n_switches++;
        }
        curr_pos++;
    }

    return n_switches;
}
