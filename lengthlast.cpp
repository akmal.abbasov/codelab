int Solution::lengthOfLastWord(const string &A) {
    // Do not write main() function.
    // Do not read input, instead use the arguments to the function.
    // Do not print the output, instead return values as specified
    // Still have a doubt. Checkout www.interviewbit.com/pages/sample_codes/ for more details
    int len = 0, b = 0;
    for (int i = 0; i < A.length(); i++) {
        if (A[i] == ' ')
            continue;
        
        b = i;
        while (A[i] != ' ' && i < A.length())
            i++;

        len = i - b;
    }
    
    return len;
}
